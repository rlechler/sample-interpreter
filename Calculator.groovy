import net.oodeveloper.sample.interpreter.ICalculator

class Calculator implements ICalculator {

    long add(long a, long b) {

        return a + b
    }

    String getInfo() {
        return "Groovy Implementation";
    }
}
