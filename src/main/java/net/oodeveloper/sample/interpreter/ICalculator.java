package net.oodeveloper.sample.interpreter;

public interface ICalculator {

    long add(long a, long b);

    String getInfo();
}
