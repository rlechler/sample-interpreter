package net.oodeveloper.sample.interpreter;

public class Calculator implements ICalculator {

    public long add(long a, long b) {

        return a + b;
    }

    public String getInfo() {
        return "Java Implementation";
    }
}
