package net.oodeveloper.sample.interpreter;

import groovy.lang.GroovyClassLoader;
import org.python.core.PyInstance;
import org.python.core.PyInteger;
import org.python.util.PythonInterpreter;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class Main {

    public static void main (String[] args) throws IOException, InstantiationException, IllegalAccessException {

        double groovy = testGroovy();
        double java = testJava();
        double python = testPython();

        groovy = testGroovy();
        java = testJava();
        python = testPython();

        System.out.println("Java:   " + java);
        System.out.println("Python: " + python);
        System.out.println("Groovy: " + groovy);
    }

    private static double testGroovy() throws IOException, IllegalAccessException, InstantiationException {

        GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        Class classAdd = groovyClassLoader.parseClass(new File("Calculator.groovy"));
        ICalculator calculator = (ICalculator) classAdd.newInstance();

        System.out.println(calculator.getInfo());

        long start = new Date().getTime();
        for (int ii = 0; ii < 10000; ii++) {
            Object result = calculator.add(ii, 7);
            System.out.println(result);
        }
        long duration = new Date().getTime() - start;
        return duration / 10000.0;

    }

    private static double testJava() {

        Calculator calculator = new Calculator();

        System.out.println(calculator.getInfo());

        long start = new Date().getTime();
        for (int ii = 0; ii < 10000; ii++) {
            Object result = calculator.add(ii, 7);
            System.out.println(result);
        }
        long duration = new Date().getTime() - start;
        return duration / 10000.0;

    }

    private static double testPython() {

        PythonInterpreter.initialize(System.getProperties(),
                System.getProperties(), new String[0]);
        PythonInterpreter interpreter = new PythonInterpreter();

        interpreter.execfile("Calculator.py");
        PyInstance test = (PyInstance) interpreter.eval("Calculator" + "(\"None\")");

        long start = new Date().getTime();
        for (int ii = 0; ii < 10000; ii++) {
            Object result =test.invoke("add", new PyInteger(ii), new PyInteger(7));
            System.out.println(result);
        }
        long duration = new Date().getTime() - start;
        return duration / 10000.0;
    }
}
